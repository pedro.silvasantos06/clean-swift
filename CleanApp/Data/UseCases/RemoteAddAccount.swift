
import Foundation
import Domain

public final class RemoteAddAccount {
    private let url: URL
    private let httpClient: HttpPostClient
    
    public init(url: URL, httpClient: HttpPostClient) {
        self.url = url
        self.httpClient = httpClient
    }
    
    public func add(addAccountModel: AddAccountModel, completion: @escaping (DomainError) -> Void) {
        //error generico nao é comparavel, por isso a ideia de criar um error padronizado
        httpClient.post(to: url, with: addAccountModel.modelToData()) { error in
            completion(.unexpected)
        }
    }
}
