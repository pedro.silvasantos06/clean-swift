//
//  DataTests.swift
//  DataTests
//
//  Created by Virtual Machine on 17/05/23.
//

import XCTest
import Domain
import Data

final class RemoteAddAccountTests: XCTestCase {

    func test_add_should_call_httpClient_with_correct_url() {
        let url = URL(string: "http://any-url.com")!
        let (sut, httpClientSpy) = makeSut(url: url)
        sut.add(addAccountModel: makeAddAccountModel()) { _ in}
        XCTAssertEqual(httpClientSpy.urls, [url])
       // XCTAssertEqual(httpClientSpy.callsCount, 1)
    }
    
    func test_add_should_call_httpClient_with_correct_data() {
        let (sut, httpClientSpy) = makeSut()
        let addAccountModel = makeAddAccountModel()
        sut.add(addAccountModel: addAccountModel) { _ in }
        //transformando um Model em Data
        XCTAssertEqual(httpClientSpy.data, addAccountModel.modelToData())
    }
    
    func test_add_should_not_complete_with_error_if_client_fails() {
        let (sut, httpClientSpy) = makeSut()
        ///testes assincronos precisamos utilizar o expectations
        let exp = expectation(description: "wainting")
        sut.add(addAccountModel: makeAddAccountModel()) { error in
            
            XCTAssertEqual(error, .unexpected)
            exp.fulfill()
        }
        httpClientSpy.completionWithError(.noConectiveError)
        ///assim que o fulfill for chamado, para de aguardar e sai do metodo
        wait(for: [exp], timeout: 1)
    }
}


//helpers
extension RemoteAddAccountTests {
    ///Factory
    //retornando uma Tupla
    func makeSut(url: URL = URL(string: "http://any-url.com")!) -> (sut: RemoteAddAccount, httpClientSpy: HttpClientSpy) {
        let httpClientSpy = HttpClientSpy()
        let sut = RemoteAddAccount(url: url, httpClient: httpClientSpy)
        return (sut, httpClientSpy)
    }
    
    func makeAddAccountModel() -> AddAccountModel {
        return AddAccountModel(name: "Pedro", email: "any_email@.com", password: "any_password", passwordConfirmation: "any_password")
    }
    
    class HttpClientSpy: HttpPostClient {
        var urls = [URL]()
        var data: Data?
        var completion: ((HttpError) -> Void)?
        func post(to url: URL, with data: Data?, completion: @escaping (HttpError) -> Void) {
            self.urls.append(url)
            self.data = data
            self.completion = completion
            
        }
        
        func completionWithError(_ error: HttpError) {
            completion?(error)
        }
    }
}
